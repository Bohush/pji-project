package com.boh0124.frogger.Groups;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.boh0124.frogger.Objects.Direction;
import com.boh0124.frogger.Objects.GameObject;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractObjectGroup extends Actor {
    protected List<GameObject> objects = new ArrayList<GameObject>();

    protected int lane;
    protected float speed;
    protected Direction direction;

    protected float obj_width;
    protected float padding;

    public AbstractObjectGroup(int lane, float speed, Direction direction, float padding) {
        this.lane = lane;
        this.speed = speed;
        this.direction = direction;
        this.padding = padding;
    }

    public float getTotalWidth() {
        int count = objects.size();
        // count-1: because I'm checking inner side of total width, so i need to subtract one width
        return obj_width*(count-1) + padding*count;
    }

    public int getLane() {
        return lane;
    }

    public boolean collidesWith(GameObject other) {
        return getCollidingObject(other) != null;
    }

    public GameObject getCollidingObject(GameObject other) {
        for (GameObject obj : objects) {
            if (obj.collidesWith(other))
                return obj;
        }
        return null;
    }

    @Override
    public void act(float delta) {
        for (GameObject obj : objects) {
            obj.act(delta);

            switch (direction) {
                case LEFT:
                    if (obj.getRight() < Math.min(Gdx.graphics.getWidth() - getTotalWidth(), 0)) {
                        obj.setX(Gdx.graphics.getWidth());
                    }
                    break;
                case RIGHT:
                    if (obj.getX() > Math.max(getTotalWidth(), Gdx.graphics.getWidth())) {
                        obj.setX(-obj_width);
                    }
                    break;
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        for (GameObject obj : objects) {
            if (obj.isVisible()) {
                obj.draw(batch, parentAlpha);
            }
        }
    }
}
