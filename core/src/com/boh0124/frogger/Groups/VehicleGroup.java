package com.boh0124.frogger.Groups;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.boh0124.frogger.Objects.Direction;
import com.boh0124.frogger.Objects.MovingObject;

public class VehicleGroup extends AbstractObjectGroup {
    public VehicleGroup(int count, float x, float padding, int lane, float speed, Direction direction,
                        String sprite_name) {
        super(lane, speed, direction, padding);
        TextureAtlas atlas = new TextureAtlas("objects/objects.atlas");
        Sprite sprite = atlas.createSprite(sprite_name);
        obj_width = sprite.getWidth();

        for (int i = 0; i < count; i++) {
            objects.add(new MovingObject(x + i*(obj_width + padding), lane, speed, direction, new Sprite(sprite)));
        }
    }
}
