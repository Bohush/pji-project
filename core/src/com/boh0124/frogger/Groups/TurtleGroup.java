package com.boh0124.frogger.Groups;

import com.boh0124.frogger.Game;
import com.boh0124.frogger.Objects.Direction;
import com.boh0124.frogger.Objects.Turtles;

public class TurtleGroup extends AbstractObjectGroup {
    public TurtleGroup(int group_count, float x, float padding, int lane, float speed, int turtle_count) {
        super(lane, speed, Direction.LEFT, padding);

        obj_width = 32*turtle_count;

        // set one turtle to be sinkable
        int sinkable = Game.RNG.nextInt(group_count);

        for (int i = 0; i < group_count; i++) {
            objects.add(new Turtles(turtle_count, x + i*(obj_width + padding), lane, speed, i == sinkable));
        }
    }
}
