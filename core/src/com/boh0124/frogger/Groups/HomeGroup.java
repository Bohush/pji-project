package com.boh0124.frogger.Groups;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.boh0124.frogger.Game;
import com.boh0124.frogger.Objects.Direction;
import com.boh0124.frogger.Objects.GameObject;

public class HomeGroup extends AbstractObjectGroup {

    public HomeGroup() {
        super(13, 0, Direction.UP, 64);
        Sprite sprite = new Sprite(new Texture("home.png"));
        obj_width = sprite.getWidth();

        for (int i = 0; i < 5; i++) {
            GameObject obj = new GameObject(16 + i*(obj_width + padding), lane * Game.ROW_SIZE, new Sprite(sprite));
            obj.setPadding(12);
            obj.setVisible(false);
            objects.add(obj);
        }
    }
}
