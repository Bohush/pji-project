package com.boh0124.frogger;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.boh0124.frogger.Stages.AbstractStage;
import com.boh0124.frogger.Stages.HighScoreStage;
import com.boh0124.frogger.Stages.MainMenuStage;
import com.boh0124.frogger.Stages.PlayStage;

import java.io.File;
import java.io.FileWriter;
import java.util.*;

public class Game extends ApplicationAdapter {
	public static final Random RNG = new Random();
	public static final int ROW_SIZE = 32;
	public static final int FONT_SIZE = 16;

	private static final String HIGHSCORES_FILE = "highscores.txt";

	private GameState state;
    private AbstractStage stage, last_stage;

    private List<Integer> highscores = new ArrayList<Integer>();

	@Override
	public void create () {
		readHighscores(true);
	    changeStage(GameState.MAIN_MENU);
	}

	public List<Integer> readHighscores() {
		return this.readHighscores(false);
	}

	public List<Integer> readHighscores(boolean force_read) {
		if (force_read || highscores.size() == 0) {
			try {
				highscores.clear();
				Scanner input = new Scanner(new File(HIGHSCORES_FILE));
				while (input.hasNextLine()) {
					String line = input.nextLine();
					highscores.add(Integer.parseInt(line));
				}
				input.close();

				Collections.sort(highscores);
				Collections.reverse(highscores);
			} catch (Exception ex) {
			    // no file found - we can ignore this, because we will just create a new one
			}
		}
		return highscores;
	}

	public void writeHighscores() {
		this.writeHighscores(highscores);
	}

	public void writeHighscores(List<Integer> highscores) {
		// write top 5 scores
		Collections.sort(highscores);
		Collections.reverse(highscores);
		highscores = highscores.size() > 5 ? highscores.subList(0, 5) : highscores;

		if (!highscores.equals(this.highscores))
			this.highscores = highscores;

		try {
			FileWriter writer = new FileWriter(HIGHSCORES_FILE);
			for (Integer score : highscores) {
				writer.write(String.format("%d\r\n", score));
			}
			writer.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void changeStage(GameState state) {
		last_stage = stage;
		// dispose stage before changing it
		if (stage != null) {
			stage.dispose();
		}

	    // change to new stage
		switch (state) {
			case MAIN_MENU:
				stage = new MainMenuStage(this);
				break;
			case IN_GAME:
				stage = new PlayStage(this);
				break;
			case HIGH_SCORES:
				stage = new HighScoreStage(this);
				break;
			default:
				stage = null;
				break;
		}
		this.state = state;
	}

	@Override
	public void render () {
		float delta = Gdx.graphics.getDeltaTime();
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
	}

	public void exit() {
		Gdx.app.exit();
	}

	public AbstractStage getLastStage() {
		return last_stage;
	}

	public int getHighscore() {
		return highscores.size() > 0 ? highscores.get(0) : 0;
	}
}
