package com.boh0124.frogger.Stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Timer;
import com.boh0124.frogger.Game;
import com.boh0124.frogger.GameState;
import com.boh0124.frogger.Groups.*;
import com.boh0124.frogger.Objects.*;

import java.util.ArrayList;
import java.util.List;

public class PlayStage extends AbstractStage {
    private Sound sfx_next;

    private TextureRegion tex_home, tex_grass, tex_life, tex_water, tex_black;

    private Label lbl_highscore, lbl_score, lbl_info;
    private Label debug_fps, debug_player, debug_danger;
    private Boolean show_debug = false;

    private int lives = 3;
    private int homes = 0;
    private int difficulty = 0;

    private int score = 0;
    private int best_lane = 1;
    private boolean extra_life;

    private boolean game_over, paused;

    private Frogger player;
    private GameTime timer;
    private List<AbstractObjectGroup> objects = new ArrayList<AbstractObjectGroup>();

    public PlayStage(Game game) {
        super(game);
        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        // Load sounds
        sfx_next = Gdx.audio.newSound(Gdx.files.internal("sounds/confirm.mp3"));

        // Load textures for background and ui
        tex_home = new TextureRegion(new Texture("terrain/grass_green.png"));
        tex_grass = new TextureRegion(new Texture("terrain/grass_purple.png"));
        tex_life = new TextureRegion(new Texture("life.png"));

        // Generate water texture
        Pixmap pixmap = new Pixmap(width, height / 2, Pixmap.Format.RGBA8888);
        pixmap.setColor(0x000047ff);
        pixmap.fillRectangle(0, 0, width, height / 2);
        tex_water = new TextureRegion(new Texture(pixmap));
        pixmap.dispose();

        // Generate black background for some labels
        pixmap = new Pixmap(1, Game.FONT_SIZE, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.BLACK);
        pixmap.fillRectangle(0, 0, 1, Game.FONT_SIZE);
        tex_black = new TextureRegion(new Texture(pixmap));
        pixmap.dispose();

        // Create info label in the middle
        lbl_info = new Label("INFO", style);
        lbl_info.setPosition(0, 15*Game.FONT_SIZE);
        lbl_info.setVisible(false);

        // Create timer and game objects
        timer = new GameTime(9*Game.FONT_SIZE, 0, 240, 30);
        addActor(timer);

        initObjects(difficulty);


        // Create labels for highscore, score and time
        Label lbl_highscore_txt = new Label("HI-SCORE", style);
        lbl_highscore_txt.setPosition(Game.FONT_SIZE * 10, height - Game.FONT_SIZE);
        addActor(lbl_highscore_txt);

        Label lbl_score_txt = new Label("1-UP", style);
        lbl_score_txt.setPosition(Game.FONT_SIZE * 4, height - Game.FONT_SIZE);
        addActor(lbl_score_txt);

        Label lbl_time_txt = new Label("TIME", style);
        lbl_time_txt.setColor(Color.YELLOW);
        lbl_time_txt.setPosition(width - 4*Game.FONT_SIZE,  0);
        addActor(lbl_time_txt);


        // Create dynamic labels for highscore and score
        lbl_highscore = new Label(String.format("%05d", game.getHighscore()), style);
        lbl_highscore.setColor(Color.RED);
        lbl_highscore.setPosition(Game.FONT_SIZE * 11, height - 2*Game.FONT_SIZE);
        addActor(lbl_highscore);

        lbl_score = new Label("00000", style);
        lbl_score.setColor(Color.RED);
        lbl_score.setPosition(Game.FONT_SIZE * 3, height - 2*Game.FONT_SIZE);
        addActor(lbl_score);


        // Create debug labels
        debug_fps = new Label("00", style);
        debug_fps.setColor(Color.CYAN);
        debug_fps.setPosition(width - 2*Game.FONT_SIZE, height - Game.FONT_SIZE);
        debug_fps.setVisible(show_debug);
        addActor(debug_fps);

        debug_player = new Label("000 00", style);
        debug_player.setColor(Color.CYAN);
        debug_player.setPosition(0, 0);
        debug_player.setVisible(show_debug);
        addActor(debug_player);

        debug_danger = new Label("OK", style);
        debug_danger.setColor(Color.GREEN);
        debug_danger.setPosition(width - 2*Game.FONT_SIZE, height - 2*Game.FONT_SIZE);
        debug_danger.setVisible(show_debug);
        addActor(debug_danger);

        initPlayer();
    }

    private int getRandomNum(int from, int to) {
        return Game.RNG.nextInt(to - from) + from;
    }

    private void initObjects(int difficulty) {
        // Reset all things
        clearObjects();

        int width = Gdx.graphics.getWidth();

        // calculate speed based on difficulty
        float speed = 1 + (0.25f * difficulty);
        // extra objects every level except 1st and every 5th
        int extra_obj = difficulty % 5 == 0 ? 0 : 1;

        // Create objects
        //yellow car
        objects.add(new VehicleGroup(
                3 + extra_obj,
                getRandomNum(0, width / 2),
                96,
                2,
                getRandomNum(45, 50) * speed,
                Direction.LEFT,
                "car_yellow"
        ));

        //white digger
        objects.add(new VehicleGroup(
                3 + extra_obj,
                getRandomNum(0, width / 2) - width/2,
                96,
                3,
                getRandomNum(30, 40) * speed,
                Direction.RIGHT,
                "digger_white"
        ));

        //pink car
        objects.add(new VehicleGroup(
                3 + extra_obj,
                getRandomNum(0, width / 2),
                128,
                4,
                getRandomNum(45, 50) * speed,
                Direction.LEFT,
                "car_pink"
        ));

        //white car
        objects.add(new VehicleGroup(
                1 + extra_obj,
                getRandomNum(0, width / 2) - width/2,
                48,
                5,
                getRandomNum(80, 150) * speed,
                Direction.RIGHT,
                "car_white"
        ));

        //white truck
        objects.add(new VehicleGroup(
                2 + extra_obj,
                getRandomNum(0, width / 2),
                96,
                6,
                getRandomNum(50, 60) * speed,
                Direction.LEFT,
                "truck_white"
        ));

        //turtles 1
        objects.add(new TurtleGroup(
                4 - extra_obj,
                getRandomNum(0, width / 2),
                48 + (extra_obj > 0 ? 32 : 0), // bigger padding with fewer turtles
                8,
                getRandomNum(40, 60) * speed,
                3
        ));

        //small log
        objects.add(new PlatformGroup(
                3,
                getRandomNum(0, width / 2) - width/2,
                80,
                9,
                getRandomNum(45, 55) * speed,
                Direction.RIGHT,
                "log_small"
        ));

        //large log
        objects.add(new PlatformGroup(
                3 - extra_obj,
                getRandomNum(0, width / 2) - width/2,
                80,
                10,
                getRandomNum(70, 80) * speed,
                Direction.RIGHT,
                "log_large"
        ));

        //turtles 2
        objects.add(new TurtleGroup(
                4 + extra_obj,
                getRandomNum(0, width / 2),
                48 - (extra_obj > 0 ? 16 : 0), // smalled padding with more turtles
                11,
                getRandomNum(45, 55) * speed,
                2
        ));

        //medium log
        objects.add(new PlatformGroup(
                4,
                getRandomNum(0, width / 2) - width/2,
                48,
                12,
                getRandomNum(50, 60) * speed,
                Direction.RIGHT,
                "log_medium"
        ));

        //home
        homes = 0;
        objects.add(new HomeGroup());

        // Add all objects as actors
        for (AbstractObjectGroup obj_group : objects) {
            addActor(obj_group);
        }
    }

    private void clearObjects() {
        for (AbstractObjectGroup obj_group : objects) {
            obj_group.remove();
        }
        objects.clear();
    }

    private void respawnPlayer() {
        if (lives <= 0) {
            if (game_over)
                return;

            game_over = true;
            setInfo("GAME OVER", Color.RED);

            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    // TODO: submit new score
                    game.changeStage(GameState.HIGH_SCORES);
                }
            }, 2f);
            return;
        }
        lives--;
        initPlayer();
    }

    private void initPlayer() {
        if (player != null) {
            player.remove();
            player.dispose();
        }
        player = new Frogger(Game.ROW_SIZE * 7, Game.ROW_SIZE);
        addActor(player);

        best_lane = 1;

        timer.resetTimer();
        timer.startTimer();
    }

    private void setDanger(String text) {
        // sets danger text to debug label
        text = text.toUpperCase();
        if (text.equals(debug_danger.getText().toString())) {
            return;
        }

        debug_danger.setText(text);
        debug_danger.setX(Gdx.graphics.getWidth() - text.length()*Game.FONT_SIZE);
        debug_danger.setColor(text.equals("OK") ? Color.GREEN : Color.RED);
    }

    private void setInfo(String text, Color color) {
        text = text.toUpperCase();

        lbl_info.setText(text);
        lbl_info.setColor(color);
        int width = Gdx.graphics.getWidth() / Game.FONT_SIZE;
        lbl_info.setX((width / 2 - text.length()/2) * Game.FONT_SIZE);
        lbl_info.setVisible(true);
        tex_black.setRegionWidth(text.length() * Game.FONT_SIZE);
    }

    @Override
    public void act(float delta) {
        // Stuff updated at all time (including while paused)

        // Toggle Debug
        if (Gdx.input.isKeyJustPressed(Input.Keys.F5)) {
            show_debug = !show_debug;
            debug_fps.setVisible(show_debug);
            debug_player.setVisible(show_debug);
            debug_danger.setVisible(show_debug);
        }

        // Debug exit
        if (show_debug && paused && Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            game.changeStage(GameState.MAIN_MENU);
        }

        // Toggle pause
        if (Gdx.input.isKeyJustPressed(Input.Keys.P) || Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            paused = !paused;
            if (paused)
                setInfo("PAUSED", Color.YELLOW);
            else
                lbl_info.setVisible(false);
        }

        // Update fps
        if (show_debug) {
            int fps = Math.min(Gdx.graphics.getFramesPerSecond(), 99);
            debug_fps.setText(String.format("%02d", fps));
        }

        if (paused)
            return;
        // Stuff updated while not paused

        String danger = "OK";

        // Control Frogger
        if (player.isAlive() && player.isVisible()) {
            if (Gdx.input.isKeyJustPressed(Input.Keys.W) || Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
                player.jump(Direction.UP);
            } else if (Gdx.input.isKeyJustPressed(Input.Keys.D) || Gdx.input.isKeyJustPressed(Input.Keys.RIGHT)) {
                player.jump(Direction.RIGHT);
            } else if (Gdx.input.isKeyJustPressed(Input.Keys.S) || Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
                player.jump(Direction.DOWN);
            } else if (Gdx.input.isKeyJustPressed(Input.Keys.A) || Gdx.input.isKeyJustPressed(Input.Keys.LEFT)) {
                player.jump(Direction.LEFT);
            }
        }

        // Debug stuff
        if (show_debug) {
            if (Gdx.input.isKeyJustPressed(Input.Keys.R)) {
                initObjects(difficulty);
                initPlayer();
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.T)) {
                if (timer.isRunning()) {
                    timer.stopTimer();
                } else {
                    timer.startTimer();
                }
            }
        }

        // Update all actors
        super.act(delta);

        // Player checks
        // Collision - objects
        for (AbstractObjectGroup obj_group : objects) {
            // check objects only on current lane
            if (obj_group.getLane() != player.getLane() || !player.isVisible()) {
                continue;
            }

            GameObject obj = obj_group.getCollidingObject(player);
            if (obj != null) {
                if (obj instanceof Platform) {
                    // jump on platform
                    player.setPlatform((Platform) obj);
                } else if (obj instanceof MovingObject) {
                    // it's a vehicle or something else dangerous
                    danger = "VEHICLE";
                } else if (!obj.isVisible()) {
                    // this should be home
                    // can only enter on free space
                    timer.stopTimer();
                    score += 50 + 20*timer.getTime();

                    obj.setVisible(true);
                    homes++;

                    // next level
                    if (homes >= 5) {
                        player.setVisible(false);
                        sfx_next.play(1f);
                        score += 1000;

                        // reset player after 500ms
                        Timer.schedule(new Timer.Task() {
                            @Override
                            public void run() {
                                initObjects(++difficulty);
                                initPlayer();
                            }
                        }, 1.5f);
                    } else {
                        initPlayer();
                    }
                }
            }
        }
        // Collision - water
        if (player.getLane() >= 8 && !player.isSafeOnWater()) {
            danger = "WATER";
        }
        // Out of time check
        if (timer.isFinished()) {
            danger = "TIME";
        }

        // Kill player
        if (player.isAlive() && player.isVisible() && !danger.equals("OK")) {
            player.kill(danger.equals("WATER"));
            timer.stopTimer();
        }
        // Respawn player
        if (!player.isAlive() && !player.isVisible()) {
            respawnPlayer();
        }

        // Update score
        if (player.isAlive() && best_lane < player.getLane() && player.getLane() != 7) {
            best_lane = player.getLane();
            score += 10;
        }
        lbl_score.setText(String.format("%05d", score));
        lbl_highscore.setText(String.format("%05d", Math.max(score, game.getHighscore())));

        // extra life at 20k
        if (score >= 20000 && !extra_life) {
            extra_life = true;
            lives++;
        }

        // Update debug labels
        if (show_debug) {
            String player_pos = String.format("%03d %02d", (int) player.getX(), player.getLane());
            debug_player.setText(player_pos);
            setDanger(danger);
        }
    }

    @Override
    public void draw() {
        // draw background
        Batch batch = getBatch();
        batch.begin();
        batch.draw(tex_water, 0, Game.ROW_SIZE * 8);
        batch.draw(tex_grass, 0, Game.ROW_SIZE);
        batch.draw(tex_grass, 0, Game.ROW_SIZE * 7);
        batch.draw(tex_home, 0, Game.ROW_SIZE * 13);
        batch.end();

        // draw everything else
        super.draw();

        // draw lives
        batch.begin();
        for (int i = 0; i < lives; i++) {
            batch.draw(tex_life, i * tex_life.getRegionWidth(), Game.FONT_SIZE);
        }

        // draw info label
        if (lbl_info.isVisible()) {
            batch.draw(tex_black, lbl_info.getX(), lbl_info.getY());
            lbl_info.draw(batch, 1);
        }
        batch.end();
    }

    @Override
    public void dispose() {
        super.dispose();
        sfx_next.dispose();
    }

    public int getScore() {
        return score;
    }
}
