package com.boh0124.frogger.Stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.boh0124.frogger.Game;
import com.boh0124.frogger.GameState;

import java.util.List;

public class HighScoreStage extends AbstractStage {
    public HighScoreStage(Game game) {
        super(game);

        Label lbl = new Label("SCORE RANKING", style);
        lbl.setColor(Color.YELLOW);
        lbl.setPosition(8 * Game.FONT_SIZE, 19 * Game.FONT_SIZE);
        addActor(lbl);

        // Update highscores
        int score = -1;
        if (game.getLastStage() instanceof PlayStage) {
            // TODO: new possible highscore
            score = ((PlayStage)game.getLastStage()).getScore();
            List<Integer> highscores = game.readHighscores();
            if (!highscores.contains(score)) {
                highscores.add(score);
                game.writeHighscores(highscores);
            }
        }
        // Get highscores
        List<Integer> highscores = game.readHighscores();

        if (highscores.size() > 0) {
            // Print top 5 scores
            String[] pos = {"ST", "ND", "RD", "TH", "TH"}; // quick and easy way
            for (int i = 0; i < Math.min(highscores.size(), 5); i++) {
                String text = String.format("%d %s   %05d PTS", i + 1, pos[i], highscores.get(i));
                lbl = new Label(text, style);
                lbl.setColor(highscores.get(i) == score ? Color.valueOf("FF47F7") : Color.WHITE);
                lbl.setPosition(7 * Game.FONT_SIZE, (16 - 2 * i) * Game.FONT_SIZE);
                addActor(lbl);
            }
        } else {
            // No scores registered
            lbl = new Label("NO GAMES PLAYED YET", style);
            lbl.setColor(Color.WHITE);
            lbl.setPosition(5 * Game.FONT_SIZE, 16 * Game.FONT_SIZE);
            addActor(lbl);
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE) || Gdx.input.isKeyJustPressed(Input.Keys.ENTER)
            || Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            game.changeStage(GameState.MAIN_MENU);
        }
    }

    @Override
    public void draw() {
        super.draw();
    }
}
