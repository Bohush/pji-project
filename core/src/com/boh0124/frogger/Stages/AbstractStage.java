package com.boh0124.frogger.Stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.boh0124.frogger.Game;

public abstract class AbstractStage extends Stage {
    protected final Game game;
    protected final Label.LabelStyle style;

    public AbstractStage(Game game) {
        super(new ScreenViewport());
        this.game = game;

        style = new Label.LabelStyle();
        style.font = new BitmapFont(Gdx.files.internal("font/frogger.fnt"));
    }
}
