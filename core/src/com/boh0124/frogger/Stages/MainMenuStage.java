package com.boh0124.frogger.Stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.boh0124.frogger.Game;
import com.boh0124.frogger.GameState;

import java.util.ArrayList;
import java.util.List;

public class MainMenuStage extends AbstractStage {
    private String[] menu = {"START GAME", "HIGH SCORES", "EXIT"};
    private List<Label> menu_labels = new ArrayList<Label>();
    private int position = 0;

    private Sound sfx_select, sfx_confirm;

    public MainMenuStage(Game game) {
        super(game);
        // Frogger logo
        Image img_logo = new Image(new Texture("logo.png"));
        img_logo.setPosition(Gdx.graphics.getWidth() / 2 - img_logo.getWidth() / 2,
                Gdx.graphics.getHeight() - img_logo.getHeight() - 32);
        img_logo.setAlign(Align.top);
        addActor(img_logo);

        // Menu
        int pad = 32;
        for (int i = 0; i < menu.length; i++) {
            Label lbl = new Label(menu[i], style);
            lbl.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            lbl.setPosition(0, (pad * (menu.length-1))/2 - i*pad);
            lbl.setAlignment(Align.center);
            addActor(lbl);
            menu_labels.add(lbl);
        }

        // Menu sounds
        sfx_select = Gdx.audio.newSound(Gdx.files.internal("sounds/select.mp3"));
        sfx_confirm = Gdx.audio.newSound(Gdx.files.internal("sounds/confirm.mp3"));

        // Author
        Label lbl_info = new Label("BOH0124  @  2018   ", style);
        lbl_info.setSize(Gdx.graphics.getWidth(), 16);
        lbl_info.setPosition(0, 32);
        lbl_info.setAlignment(Align.center);
        addActor(lbl_info);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        int prev_position = position;
        // Move in menu
        if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN) || Gdx.input.isKeyJustPressed(Input.Keys.S)) {
            position = Math.min(position + 1, menu.length - 1);
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.UP) || Gdx.input.isKeyJustPressed(Input.Keys.W)) {
            position = Math.max(position - 1, 0);
        }
        // Update label colors
        if (prev_position != position) {
            sfx_select.play(0.25f);
            menu_labels.get(prev_position).setColor(Color.WHITE);
        }
        menu_labels.get(position).setColor(Color.YELLOW);

        // Do the action
        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE) || Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            sfx_confirm.play(1f);
            String opt = menu[position];
            if (opt.equals("START GAME")) {
                game.changeStage(GameState.IN_GAME);
            } else if (opt.equals("HIGH SCORES")) {
                game.changeStage(GameState.HIGH_SCORES);
            } else if (opt.equals("EXIT")) {
                // Wait for sound to finish
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        game.exit();
                    }
                }, 0.4f);
            }
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            game.exit();
        }
    }

    @Override
    public void draw() {
        super.draw();
    }

    @Override
    public void dispose() {
        // dispose sounds after 500ms - to let them finish playing
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                sfx_select.dispose();
                sfx_confirm.dispose();
            }
        }, 0.5f);
        super.dispose();
    }
}
