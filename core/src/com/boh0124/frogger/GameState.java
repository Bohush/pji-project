package com.boh0124.frogger;

public enum GameState {
    MAIN_MENU,
    IN_GAME,
    HIGH_SCORES
}
