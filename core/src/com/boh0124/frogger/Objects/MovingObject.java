package com.boh0124.frogger.Objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.boh0124.frogger.Game;

public class MovingObject extends GameObject {
    protected int lane;
    protected float speed;
    protected Direction direction;

    protected boolean loop = false;

    public MovingObject(float x, int lane, float speed, Direction direction, Sprite sprite) {
        this(x, lane, speed, direction, sprite, false);
    }

    public MovingObject(float x, int lane, float speed, Direction direction, Sprite sprite, boolean loop) {
        super(x, lane * Game.ROW_SIZE, sprite);
        this.lane = lane;
        this.speed = speed;
        this.direction = direction;
        this.loop = loop;
        padding = 8;
    }

    public int getLane() {
        return lane;
    }

    public void setLooping(boolean value) {
        this.loop = value;
    }

    @Override
    public void act(float delta) {
        float dx = speed * delta * direction.getDx();
        moveBy(dx, 0);

        if (loop) {
            switch (direction) {
                case LEFT:
                    if (getRight() < 0)
                        setX(Gdx.graphics.getWidth());
                    break;
                case RIGHT:
                    if (getX() > Gdx.graphics.getWidth())
                        setX(-getWidth());
                    break;
            }
        }
    }
}
