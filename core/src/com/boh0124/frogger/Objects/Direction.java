package com.boh0124.frogger.Objects;

public enum Direction {
    UP(0, 0, 1),
    LEFT(90, -1, 0),
    DOWN(180, 0, -1),
    RIGHT(270, 1, 0);

    private int degrees, dx, dy;

    private Direction(int degrees, int dx, int dy) {
        this.degrees = degrees;
        this.dx = dx;
        this.dy = dy;
    }

    public int getDegrees() {
        return degrees;
    }

    public int getDx() {
        return dx;
    }

    public int getDy() {
        return dy;
    }
}
