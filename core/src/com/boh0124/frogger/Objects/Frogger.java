package com.boh0124.frogger.Objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Timer;
import com.boh0124.frogger.Game;


public class Frogger extends GameObject implements Disposable {
    private TextureRegion tex_idle, tex_jump;
    private Sound sfx_jump, sfx_death;

    Animation<TextureRegion> anim_death, anim_death_car, anim_death_water;
    private float death_time = 0;
    private boolean is_alive = true;

    private final float speed = 300;
    private final float jump_distance = Game.ROW_SIZE;

    private Direction direction;
    private Boolean is_jumping = false;
    private float distance = 0;

    private Platform platform;

    public Frogger() {
        this(0, 0);
    }

    public Frogger(float x, float y) {
        TextureAtlas atlas = new TextureAtlas("objects/frogger.atlas");
        tex_idle = atlas.findRegion("idle");
        tex_jump = atlas.findRegion("jump");

        sprite = new Sprite(tex_idle);
        setBounds(x, y, Game.ROW_SIZE, Game.ROW_SIZE);
        setDirection(Direction.UP);

        anim_death_car = new Animation<TextureRegion>(1f/4, atlas.findRegions("death_car"));
        anim_death_water = new Animation<TextureRegion>(1f/4, atlas.findRegions("death_water"));

        sfx_jump = Gdx.audio.newSound(Gdx.files.internal("sounds/jump.wav"));
        sfx_death = Gdx.audio.newSound(Gdx.files.internal("sounds/death.wav"));
    }

    private void setDirection(Direction dir) {
        this.direction = dir;
        sprite.setRotation(dir.getDegrees());
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public boolean isSafeOnWater() {
        if (getRight() < 0 || getX() > Gdx.graphics.getWidth()) {
            return false;
        }
        return platform != null && platform.isVisible();
    }

    public boolean isAlive() {
        return is_alive;
    }

    public int getLane() {
        return Math.round(getY() / Game.ROW_SIZE);
    }


    public void kill(boolean sink) {
        anim_death = sink ? anim_death_water : anim_death_car;
        is_alive = false;
        sfx_death.play(0.6f);
    }

    public void jump(Direction dir) {
        if (is_jumping)
            return;

        // Prevent out of bounds
        // calculate new position
        float x = getX() + dir.getDx() * jump_distance;
        float y = getY() + dir.getDy() * jump_distance;
        // calculate distance from edge (based on direction)
        float diff = 0;
        switch (dir) {
            case UP:
                diff = y - (Gdx.graphics.getHeight() - 3*Game.ROW_SIZE);
                break;
            case LEFT:
                diff = x * -1;
                break;
            case DOWN:
                diff = (y - Game.ROW_SIZE) * -1;
                break;
            case RIGHT:
                diff = x - (Gdx.graphics.getWidth() - Game.ROW_SIZE);
                break;
        }
        // subtract the difference (diff will be negative if new position is inside the window)
        distance = jump_distance - Math.max(diff, 0);

        // only start jumping if there is any distance
        is_jumping = distance > 0;
        if (is_jumping) {
            setDirection(dir);
            sprite.setRegion(tex_jump);
            // play sound with slight variation
            float pitch = 1.0f + (Game.RNG.nextFloat() - 0.5f) / 5;
            sfx_jump.play(0.5f, pitch, 0);
        }
    }

    @Override
    public void act(float delta) {
        if (!is_alive) {
            death_time += delta;
            return;
        }

        // animate jumping
        if (is_jumping) {
            float dpos = speed * delta;
            // move by difference or the rest of remaining distance
            dpos = Math.min(dpos, distance);
            moveBy(direction.getDx() * dpos, direction.getDy() * dpos);
            distance -= dpos;
            if (distance <= 0) {
                distance = 0;
                is_jumping = false;
                sprite.setRegion(tex_idle);
                // fix slight position offset
                setPosition(Math.round(getX()), Math.round(getY()));
            }
        }

        // move on platform
        if (platform != null) {
            moveBy(platform.getDx(), 0);

            // remove platform
            if (!platform.getHitbox().overlaps(this.getHitbox())) {
                platform = null;
                // round X position
                setPosition(Math.round(getX()), getY());
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (!isVisible())
            return;

        if (is_alive) {
            super.draw(batch, parentAlpha);
        } else if (anim_death != null) {
            TextureRegion texture = anim_death.getKeyFrame(death_time);
            batch.draw(texture, getX(), getY());

            if (anim_death.isAnimationFinished(death_time)) {
                setVisible(false);
            }
        }
    }

    @Override
    public void dispose() {
        // dispose after 500ms
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                sfx_jump.dispose();
                sfx_death.dispose();
            }
        },  0.5f);
    }
}
