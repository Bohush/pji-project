package com.boh0124.frogger.Objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.boh0124.frogger.Game;

public class GameTime extends Actor {
    private TextureRegion texture;

    private int start_time;
    private float time;
    private boolean is_running = false;

    public GameTime(float x, float y, int width, int time_in_seconds) {
        this.start_time = time_in_seconds;
        setBounds(x, y, width, Game.FONT_SIZE);

        // generate texture
        Pixmap pixmap = new Pixmap(1, Game.FONT_SIZE, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.GREEN);
        pixmap.fillRectangle(0, 0, 1, Game.FONT_SIZE);
        texture = new TextureRegion(new Texture(pixmap));
        pixmap.dispose();

        resetTimer();
    }

    public void startTimer() {
        is_running = true;
    }

    public void stopTimer() {
        is_running = false;
    }

    public void resetTimer() {
        time = start_time;
    }

    public boolean isRunning() {
        return is_running;
    }

    public boolean isFinished() {
        return !is_running && time == 0;
    }

    public int getMaxTime() {
        return start_time;
    }

    public int getTime() {
        return Math.round(time);
    }

    @Override
    public void act(float delta) {
        if (!is_running)
            return;

        time -= delta;
        if (time <= 0) {
            time = 0;
            stopTimer();
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        float percent = time / start_time;
        float diff = getWidth() * (1 - percent);
        batch.draw(texture, getX() + diff, getY(), getWidth() * percent, Game.FONT_SIZE);
    }
}
