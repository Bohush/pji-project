package com.boh0124.frogger.Objects;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class Platform extends MovingObject {
    private float dx = 0;

    public Platform(float x, int lane, float speed, Direction direction, Sprite sprite) {
        super(x, lane, speed, direction, sprite);
        padding = 16;
    }

    public float getDx() {
        return dx;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        dx = speed * delta * direction.getDx();
    }
}
