package com.boh0124.frogger.Objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class GameObject extends Actor {
    protected Sprite sprite;
    protected float padding = 0;

    public GameObject() {
        this(0, 0);
    }

    public GameObject(float x, float y) {
        setPosition(x, y);
    }

    public GameObject(float x, float y, Sprite sprite) {
        this.sprite = sprite;
        if (sprite != null) {
            setBounds(x, y, sprite.getWidth(), sprite.getHeight());
        } else {
            setPosition(x, y);
        }
    }

    public Rectangle getHitbox() {
        return new Rectangle(getX() + padding, getY(), getWidth() - padding*2, getHeight());
    }

    public boolean collidesWith(GameObject other) {
        return this.getHitbox().overlaps(other.getHitbox());
    }

    public void setPadding(float value) {
        this.padding = value;
    }

    @Override
    public void setBounds(float x, float y, float width, float height) {
        super.setBounds(x, y, width, height);
        if (sprite != null) {
            sprite.setBounds(x, y, sprite.getWidth(), sprite.getHeight());
        }
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        if (sprite != null) {
            sprite.setPosition(x, y);
        }
    }

    @Override
    public void setX(float x) {
        super.setX(x);
        if (sprite != null) {
            sprite.setX(x);
        }
    }

    @Override
    public void setY(float y) {
        super.setY(y);
        if (sprite != null) {
            sprite.setY(y);
        }
    }

    @Override
    public void moveBy(float x, float y) {
        super.moveBy(x, y);
        if (sprite != null) {
            sprite.setPosition(getX(), getY());
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if (sprite != null) {
            sprite.draw(batch, parentAlpha);
        }
    }
}
