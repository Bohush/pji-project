package com.boh0124.frogger.Objects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.boh0124.frogger.Game;

public class Turtles extends Platform {
    Animation<TextureRegion> anim_swim, anim_sink;

    private int count;
    private boolean sinkable;
    private boolean is_sinking = false;

    private float swim_time, sink_time;

    public Turtles(int count, float x, int lane, float speed, boolean sinkable) {
        super(x, lane, speed, Direction.LEFT, null);

        this.count = count;
        this.sinkable = sinkable;
        swim_time = 0;
        sink_time = 0;

        TextureAtlas atlas = new TextureAtlas("objects/objects.atlas");
        anim_swim = new Animation<TextureRegion>(1f/4, atlas.findRegions("turtle_swim"), Animation.PlayMode.LOOP);
        anim_sink = new Animation<TextureRegion>(1f/(speed/45), atlas.findRegions("turtle_sink"), Animation.PlayMode.NORMAL);
        // TODO: maybe use PlayMode.LOOP_PINGPONG for anim_sink

        setBounds(getX(), getY(), atlas.createSprite("turtle_swim").getWidth() * count, Game.ROW_SIZE);
    }

    public void setSinkable(boolean sinkable) {
        this.sinkable = sinkable;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        swim_time += delta;

        if (!sinkable)
            return;

        // play sinking animation
        sink_time += delta;
        is_sinking = !anim_sink.isAnimationFinished(sink_time);

        // if animation is finished
        // reverse the playmode and hide the turtles or show the swimming animation for one frame (of animation)
        if (!is_sinking) {
            // hide under water
            if (anim_sink.getPlayMode() == Animation.PlayMode.NORMAL && isVisible()) {
                setVisible(false);
            }
            // wait and extra frame and then reverse the animation
            if (anim_sink.isAnimationFinished(sink_time - anim_sink.getFrameDuration())) {
                setVisible(true);
                sink_time = 0;
                is_sinking = true;
                switch (anim_sink.getPlayMode()) {
                    case NORMAL:
                        anim_sink.setPlayMode(Animation.PlayMode.REVERSED);
                        break;
                    case REVERSED:
                        anim_sink.setPlayMode(Animation.PlayMode.NORMAL);
                        break;
                }
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (!isVisible())
            return;

        TextureRegion tex_swim = anim_swim.getKeyFrame(swim_time);
        TextureRegion tex_sink = anim_sink.getKeyFrame(sink_time);

        for (int i = 0; i < count; i++) {
            TextureRegion texture = is_sinking ? tex_sink : tex_swim;
            batch.draw(texture, getX() + i*texture.getRegionWidth(), getY());
        }
    }
}
