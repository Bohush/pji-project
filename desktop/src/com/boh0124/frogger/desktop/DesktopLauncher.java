package com.boh0124.frogger.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.boh0124.frogger.Game;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Frogger";
		cfg.height = 512;
		cfg.width = 448;
		cfg.fullscreen = false;
		cfg.resizable = false;
		cfg.addIcon("home.png", Files.FileType.Internal);
		new LwjglApplication(new Game(), cfg);
	}
}
