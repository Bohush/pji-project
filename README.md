# Frogger
Term project for "Programming Languages 1" course at [VŠB-TUO](https://www.vsb.cz/en).

## Project repository
https://gitlab.com/Bohush/pji-project

## Libraries used
[libGDX](https://libgdx.badlogicgames.com/)

## Controls
Action | Keys
--- | ---
Movement | W, A, S, D or Arrow keys
Confirm | Enter or Space
Pause | Escape or P

## Build notes
Create new run configuration based on these steps:

1. Create new Application configuration.
2. Set module/project to `desktop.main` or something similar.
3. Set main class to `DesktopLauncher`.
4. Set working directory to `/core/assets`.
5. Build!

## Credits
Game art:  
https://www.spriters-resource.com/arcade/frogger/sheet/11067/

Sounds:  
http://taira-komori.jpn.org/game01en.html  
https://freesound.org/people/OwlStorm/sounds/404743/  
https://freesound.org/people/jeckkech/sounds/391670/
